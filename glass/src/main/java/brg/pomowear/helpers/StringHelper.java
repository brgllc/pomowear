package brg.pomowear.helpers;

/**
 * Created by jmoralez on 7/28/14.
 */
public class StringHelper {

    public static boolean isNumeric(String s) {

        try {
            Long.parseLong(s);
        } catch(NumberFormatException nfe) {
            return false;
        }

        return true;
    }
}
