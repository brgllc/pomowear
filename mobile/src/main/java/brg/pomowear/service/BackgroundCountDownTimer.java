package brg.pomowear.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.os.Vibrator;
import android.support.v4.content.LocalBroadcastManager;

import brg.pomowear.R;

public class BackgroundCountDownTimer extends Service {
    private final IBinder mBinder = new BackgroundCountDownTimerBinder();

    public static final String UPDATE_TIMER_COUNTDOWN = "updateTimerCountdownText";
    public static final String UPDATE_NOTIFICATION = "updateNotification";
    public static final String TIMER_FINISHED = "timerFinished";
    public static final String SESSION_STOPPED = "sessionStopped";

    boolean sessionFinished = false;

    CountDownTimer mCountdownTimer;
    Integer sessionLength;

    @Override
    public IBinder onBind(Intent i) {
        return mBinder;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent i, int flags, int startId) {
        sessionLength = i.getIntExtra("sessionLength", 0);
        return START_STICKY;
    }

    @Override
    public boolean stopService(Intent name) {
        if (mCountdownTimer != null) {
            mCountdownTimer.cancel();
        }
        Intent sessionStoppedIntent = new Intent(SESSION_STOPPED);
        LocalBroadcastManager.getInstance(this).sendBroadcast(sessionStoppedIntent);
        return super.stopService(name);
    }

    private void updateTimerCountdownText(long millisUntilFinished) {
        Intent updateTimerCountdownTextIntent = new Intent(UPDATE_TIMER_COUNTDOWN);
        updateTimerCountdownTextIntent.putExtra(UPDATE_TIMER_COUNTDOWN, millisUntilFinished);
        LocalBroadcastManager.getInstance(this).sendBroadcast(updateTimerCountdownTextIntent);
    }

    private void updatePomoNotification(String status) {
        Intent updateNotificationIntent = new Intent(UPDATE_NOTIFICATION);
        updateNotificationIntent.putExtra(UPDATE_NOTIFICATION, status);
        LocalBroadcastManager.getInstance(this).sendBroadcast(updateNotificationIntent);
    }

    public void startTimer() {
        sessionFinished = false;
        mCountdownTimer = new CountDownTimer(sessionLength, 1000) {

            public void onTick(long millisUntilFinished) {
                updateTimerCountdownText(millisUntilFinished);
            }

            public void onFinish() {
                Intent timerFinishedIntent = new Intent(TIMER_FINISHED);
                LocalBroadcastManager.getInstance(BackgroundCountDownTimer.this).sendBroadcast(timerFinishedIntent);

                Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                // Vibrate for 500 milliseconds
                v.vibrate(new long[]{0, 500, 100, 500, 100, 500}, -1);

                updatePomoNotification(getString(R.string.session_finished));
                sessionFinished = true;
            }
        };
        mCountdownTimer.start();

        updateTimerCountdownText((long)(sessionLength - 1000));
        updatePomoNotification(getString(R.string.session_running));
    }

    public void stopTimer() {
        if (mCountdownTimer != null) {
            mCountdownTimer.cancel();
            mCountdownTimer = null;
        }
    }

    public boolean isRunning() {
        return mCountdownTimer != null;
    }

    public boolean isSessionFinished() {
        return sessionFinished;
    }

    public void setSessionLength(Integer sessionLength) {
        this.sessionLength = sessionLength;
    }

    public class BackgroundCountDownTimerBinder extends Binder {

        public BackgroundCountDownTimer getService() {
            return BackgroundCountDownTimer.this;
        }
    }
}