package brg.pomowear.ui_elements;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

public class SquareButton extends Button {
    public SquareButton(Context context) {
        super(context);
    }

    public SquareButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SquareButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);

        int buttonSize = Math.min(width, height);

        super.onMeasure(
                MeasureSpec.makeMeasureSpec(buttonSize, MeasureSpec.EXACTLY),
                MeasureSpec.makeMeasureSpec(buttonSize, MeasureSpec.EXACTLY)
        );
    }
}
