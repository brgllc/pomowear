package brg.pomowear.activity;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

import brg.pomowear.R;
import brg.pomowear.ui_elements.SquareButton;
import brg.pomowear.service.BackgroundCountDownTimer;

public class MainActivity extends BaseActivity {

    Integer sessionLength;

    SquareButton tomatoBtn;
    Button  plusBtn;
    Button  minusBtn;
    Boolean isRunning = false;
    Boolean longClicked = false;

    Timer increaseTimer;
    Timer decreaseTimer;

    BackgroundCountDownTimer countDownTimerService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        SharedPreferences sp = getSharedPreferences("brg.pomowear", Context.MODE_PRIVATE);
        sessionLength = sp.getInt("DEFAULT_SESSION_LENGTH", 25);

        tomatoBtn = (SquareButton)findViewById(R.id.timer);
        tomatoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isRunning) {
                    stopPomodoroTimer();
                } else {
                    startPomodoroTimer();
                }
            }
        });

        plusBtn = (Button)findViewById(R.id.plus_btn);
        plusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isRunning) {
                    return;
                }

                if (longClicked) {
                    longClicked = false;
                } else {
                    sessionLength += 1;
                    updateSessionLengthText();
                    saveDefaultTime();
                }
            }
        });
        plusBtn.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (isRunning) {
                    return true;
                }

                startIncreasingTime();
                return false;
            }
        });
        plusBtn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (isRunning) {
                    return true;
                }

                switch(event.getAction()) {
                    case MotionEvent.ACTION_UP:
                        stopIncreasingTime();
                        return false;
                }
                return false;
            }
        });

        minusBtn = (Button)findViewById(R.id.minus_btn);
        minusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isRunning) {
                    return;
                }

                if (longClicked) {
                    longClicked = false;
                } else {
                    sessionLength -= 1;
                    sessionLength = Math.max(sessionLength, 1);
                    updateSessionLengthText();
                    saveDefaultTime();
                }
            }
        });
        minusBtn.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (isRunning) {
                    return true;
                }

                startDecreasingTime();
                return false;
            }
        });
        minusBtn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (isRunning) {
                    return true;
                }

                switch (event.getAction()) {
                    case MotionEvent.ACTION_UP:
                        stopDecreasingTime();
                        return false;
                }
                return false;
            }
        });

        setTimerCountDownTextDefault(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();

        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter(BackgroundCountDownTimer.UPDATE_TIMER_COUNTDOWN));
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter(BackgroundCountDownTimer.UPDATE_NOTIFICATION));
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter(BackgroundCountDownTimer.TIMER_FINISHED));
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, new IntentFilter(BackgroundCountDownTimer.SESSION_STOPPED));

        if (countDownTimerService == null) {
            SharedPreferences sp = getSharedPreferences("brg.pomowear", Context.MODE_PRIVATE);
            Integer defaultSessionLength = sp.getInt("DEFAULT_SESSION_LENGTH", 25);
            Integer defaultSessionLengthInMilliseconds = defaultSessionLength * 60 * 1000;
            Intent intent = new Intent(this, BackgroundCountDownTimer.class);
            intent.putExtra("sessionLength", defaultSessionLengthInMilliseconds);
            bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
        } else if (countDownTimerService.isSessionFinished()) {
            timerFinished();
        }
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        mConnection = null;
        super.onPause();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putCharSequence("lastTimeSeen", tomatoBtn.getText());
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    private void startPomodoroTimer() {
        SharedPreferences sp = getSharedPreferences("brg.pomowear", Context.MODE_PRIVATE);
        Integer defaultSessionLength = sp.getInt("DEFAULT_SESSION_LENGTH", 25);
        Integer defaultSessionLengthInMilliseconds = defaultSessionLength * 60 * 1000;

        if (countDownTimerService != null && !isRunning) {
            countDownTimerService.setSessionLength(defaultSessionLengthInMilliseconds);
            countDownTimerService.startTimer();
            isRunning = true;
            Toast.makeText(MainActivity.this, getString(R.string.session_running), Toast.LENGTH_SHORT).show();
            updatePomoNotification(getString(R.string.session_running));
        }
    }

    private void stopPomodoroTimer() {
        if (countDownTimerService != null) {
            countDownTimerService.stopTimer();
            isRunning = false;
        }

        setTimerCountdownTextDefault(getString(R.string.session_stopped));
    }

    public void setTimerCountDownTextDefault(Bundle savedInstanceState) {
        CharSequence lastTimeSeen = null;
        if (savedInstanceState != null && savedInstanceState.containsKey("lastTimeSeen")) {
            lastTimeSeen = savedInstanceState.getCharSequence("lastTimeSeen");
        }
        setTimerCountdownTextDefault(getString(R.string.session_not_running), lastTimeSeen);

    }

    public void setTimerCountdownTextDefault(final String notificationText) {
        setTimerCountdownTextDefault(notificationText, null);
    }

    public void setTimerCountdownTextDefault(final String notificationText, final CharSequence lastTimeSeen) {
        if (tomatoBtn != null && !isRunning) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    SharedPreferences sp = MainActivity.this.getSharedPreferences("brg.pomowear", Context.MODE_PRIVATE);
                    Integer defaultSessionLength = sp.getInt("DEFAULT_SESSION_LENGTH", 25);
                    Integer hours = defaultSessionLength / 60;
                    Integer minutes = Math.max(defaultSessionLength - (hours * 60), 0);

                    if (countDownTimerService != null && countDownTimerService.isSessionFinished()) {
                        tomatoBtn.setText(getString(R.string.done));
                    } else if (lastTimeSeen != null) {
                        tomatoBtn.setText(lastTimeSeen);
                    } else {
                        tomatoBtn.setText(String.format("%d:%02d.00", hours, minutes));
                    }

                    if (!notificationText.equalsIgnoreCase(getString(R.string.session_not_running))) {
                        updatePomoNotification(notificationText);
                    }
                }
            });
        }
    }

    private void updateTimerCountdownText(final Long timeInMillis) {
        if (tomatoBtn != null) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Integer hours = timeInMillis.intValue() / (1000 * 60 * 60);
                    Integer minutes = Math.max((timeInMillis.intValue() / (1000 * 60) - (hours * 60)), 0);
                    Integer seconds = Math.max((timeInMillis.intValue() / (1000) - (minutes * 60) - (hours * 60)), 0);

                    tomatoBtn.setText(String.format("%d:%02d.%02d", hours, minutes, seconds));
//                    updatePomoNotification();
                }
            });
        }
    }

    private void updatePomoNotification(String status) {
        int notificationId = 1001;
        // Build intent for notification content
        Intent notificationIntent = new Intent(this, MainActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent viewPendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

        Intent restartTimerIntent = new Intent(Intent.ACTION_VIEW);
        restartTimerIntent.setAction("RESTART");
        PendingIntent pendingRestartIntent = PendingIntent.getActivity(this, 0, restartTimerIntent, 0);

        String notificationContentText = "";
        if (tomatoBtn != null) {
            notificationContentText = tomatoBtn.getText().toString();
        }

        Bitmap largeIcon = BitmapFactory.decodeResource(this.getResources(), R.drawable.ic_notification_large_icon);

//        NotificationCompat.Action action =
//                new NotificationCompat.Action.Builder(R.drawable.ic_refresh, "Restart", pendingRestartIntent).build();

        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setLargeIcon(largeIcon)
                        .setContentTitle(getString(R.string.app_name))
                        .setContentText(status)
                        .setContentIntent(viewPendingIntent)
//                        .setStyle(new NotificationCompat.BigPictureStyle());
                        .addAction(R.drawable.ic_refresh, "Restart", pendingRestartIntent);
//                        .extend(new NotificationCompat.WearableExtender().addAction(action))

        // Get an instance of the NotificationManager service
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);

        // Build the notification and issues it with notification manager.
        notificationManager.notify(notificationId, notificationBuilder.build());
    }

    private void startIncreasingTime() {
        int period = 500;
        longClicked = true;

        increaseTimer = new Timer();
        increaseTimer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                Integer increaseAmt = 5 - (sessionLength % 5);
                if (increaseAmt == 0) {
                    increaseAmt = 5;
                }

                sessionLength += increaseAmt;

                updateSessionLengthText();
            }
        }, 0, period);
    }

    private void stopIncreasingTime() {
        if (increaseTimer != null) {
            increaseTimer.cancel();
            increaseTimer.purge();
        }

        saveDefaultTime();
    }

    private void startDecreasingTime() {
        int period = 500;
        longClicked = true;

        decreaseTimer = new Timer();
        decreaseTimer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                Integer decreaseAmt = sessionLength % 5;
                if (decreaseAmt == 0) {
                    decreaseAmt = 5;
                }

                sessionLength -= decreaseAmt;
                if (sessionLength <= 1) {
                    sessionLength = 1;
                    stopDecreasingTime();
                }

                updateSessionLengthText();
            }
        }, 0, period);
    }

    private void stopDecreasingTime() {
        if (decreaseTimer != null) {
            decreaseTimer.cancel();
            decreaseTimer.purge();
        }

        saveDefaultTime();
    }

    public void saveDefaultTime() {
        SharedPreferences sp = getSharedPreferences("brg.pomowear", Context.MODE_PRIVATE);
        sp.edit().putInt("DEFAULT_SESSION_LENGTH", sessionLength).commit();
    }

    private void updateSessionLengthText() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Integer hours = sessionLength / 60;
                Integer minutes = Math.max(sessionLength - (hours * 60), 0);

                tomatoBtn.setText(String.format("%d:%02d.00", hours, minutes));
            }
        });
    }

    private void timerFinished() {
        if (tomatoBtn != null) {
            tomatoBtn.setText(getString(R.string.done));
        }

        updatePomoNotification(getString(R.string.session_finished));

        isRunning = false;
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equalsIgnoreCase(BackgroundCountDownTimer.UPDATE_TIMER_COUNTDOWN)) {
                long updateTime = intent.getLongExtra(BackgroundCountDownTimer.UPDATE_TIMER_COUNTDOWN, 0);
                updateTimerCountdownText(updateTime);
            } else if (intent.getAction().equalsIgnoreCase(BackgroundCountDownTimer.UPDATE_NOTIFICATION)) {
                String status = intent.getStringExtra(BackgroundCountDownTimer.UPDATE_NOTIFICATION);
                updatePomoNotification(status);
            } else if (intent.getAction().equalsIgnoreCase(BackgroundCountDownTimer.TIMER_FINISHED)) {
                timerFinished();
            } else if (intent.getAction().equalsIgnoreCase(BackgroundCountDownTimer.SESSION_STOPPED)) {
                setTimerCountdownTextDefault(getString(R.string.session_stopped));
            }
        }
    };

    private ServiceConnection mConnection = new ServiceConnection() {

        public void onServiceConnected(ComponentName className, IBinder binder) {
            BackgroundCountDownTimer.BackgroundCountDownTimerBinder b = (BackgroundCountDownTimer.BackgroundCountDownTimerBinder) binder;
            countDownTimerService = b.getService();
            if (countDownTimerService.isRunning()) {
                isRunning = true;
            }
        }

        public void onServiceDisconnected(ComponentName className) {
            countDownTimerService = null;
        }
    };
}
