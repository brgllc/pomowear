package brg.pomowear.activity;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

import brg.pomowear.R;

/**
 * Created by jmoralez on 7/23/14.
 */
public class AboutActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        TextView versionText = (TextView)findViewById(R.id.version);
        try {
            String app_ver = this.getPackageManager().getPackageInfo(this.getPackageName(), 0).versionName;
            versionText.setText(getString(R.string.version) + app_ver);
        } catch (PackageManager.NameNotFoundException e) {
            versionText.setVisibility(View.GONE);
        }

        pageId = R.id.action_about;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

}
