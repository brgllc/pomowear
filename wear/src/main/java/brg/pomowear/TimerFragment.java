package brg.pomowear;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.wearable.view.CircledImageView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TimerFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TimerFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class TimerFragment extends Fragment {

    private TextView            mTimerCountdownTxt;
    private CircledImageView    mStartBtn;
    private CircledImageView    mStopBtn;
    public  Boolean             isRunning = false;

    private CountDownTimer mCountdownTimer;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment TimerFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TimerFragment newInstance(String param1, String param2) {
        TimerFragment fragment = new TimerFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public static TimerFragment newInstance() {
        TimerFragment fragment = new TimerFragment();
        return fragment;
    }

    public TimerFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_timer, container, false);

        mStartBtn = (CircledImageView)v.findViewById(R.id.timer_countdown_start_btn);
        mStartBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isRunning) {
                    startPomodoroTimer();
                } else {
                    stopPomodoroTimer();
                }
            }
        });

        mStopBtn = (CircledImageView)v.findViewById(R.id.timer_countdown_stop_btn);
        mStopBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stopPomodoroTimer();
            }
        });

        mTimerCountdownTxt = (TextView)v.findViewById(R.id.timer_countdown_txt);
        setTimerCountdownTextDefault();

        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    public TextView getTimerCountdownTxt() {
        return mTimerCountdownTxt;
    }

    public void setTimerCountdownTextDefault() {
        if (mTimerCountdownTxt != null && !isRunning) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    SharedPreferences sp = TimerFragment.this.getActivity().getSharedPreferences("brg.pomowear", Context.MODE_PRIVATE);
                    Integer defaultSessionLength = sp.getInt("DEFAULT_SESSION_LENGTH", 25);
                    Integer hours = defaultSessionLength / 60;
                    Integer minutes = Math.max(defaultSessionLength - (hours * 60), 0);

                    mTimerCountdownTxt.setText(String.format("%d:%02d.00", hours, minutes));
                    updatePomoNotification("No Session Running");
                }
            });
        }
    }

    private void updateTimerCountdownText(final Long timeInMillis) {
        if (mTimerCountdownTxt != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Integer hours = timeInMillis.intValue() / (1000 * 60 * 60);
                    Integer minutes = Math.max((timeInMillis.intValue() / (1000 * 60) - (hours * 60)), 0);
                    Integer seconds = Math.max((timeInMillis.intValue() / (1000) - (minutes * 60) - (hours * 60)), 0);

                    mTimerCountdownTxt.setText(String.format("%d:%02d.%02d", hours, minutes, seconds));
//                    updatePomoNotification();
                }
            });
        }
    }

    private void startPomodoroTimer() {
        SharedPreferences sp = this.getActivity().getSharedPreferences("brg.pomowear", Context.MODE_PRIVATE);
        Integer defaultSessionLength = sp.getInt("DEFAULT_SESSION_LENGTH", 25);
        Integer defaultSessionLengthInMilliseconds = defaultSessionLength * 60 * 1000;

        mCountdownTimer = new CountDownTimer(defaultSessionLengthInMilliseconds, 1000) {

            public void onTick(long millisUntilFinished) {
                updateTimerCountdownText(millisUntilFinished);
            }

            public void onFinish() {
                if (mTimerCountdownTxt != null) {
                    mTimerCountdownTxt.setText("done!");
                }

                Vibrator v = (Vibrator) TimerFragment.this.getActivity().getSystemService(Context.VIBRATOR_SERVICE);
                // Vibrate for 500 milliseconds
                v.vibrate(new long[]{0, 500, 100, 500, 100, 500}, -1);

                updatePomoNotification("Session Finished");

                isRunning = false;
            }
        };

        mCountdownTimer.start();
        updateTimerCountdownText((long)(defaultSessionLengthInMilliseconds - 1000));
        updatePomoNotification("Session Running");

        mStartBtn.setImageResource(R.drawable.ic_stop);

        isRunning = true;
    }

    private void stopPomodoroTimer() {
        isRunning = false;
        if (mCountdownTimer != null) {
            mCountdownTimer.cancel();
            setTimerCountdownTextDefault();
        }
        mStartBtn.setImageResource(R.drawable.ic_play);
    }

    private void updatePomoNotification(String status) {
        int notificationId = 1001;
        // Build intent for notification content
        Intent notificationIntent = new Intent(this.getActivity(), MainWearActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent viewPendingIntent = PendingIntent.getActivity(this.getActivity(), 0, notificationIntent, 0);

        Intent restartTimerIntent = new Intent(Intent.ACTION_VIEW);
        restartTimerIntent.setAction("RESTART");
        PendingIntent pendingRestartIntent = PendingIntent.getActivity(this.getActivity(), 0, restartTimerIntent, 0);

        String notificationContentText = "";
        if (getTimerCountdownTxt() != null) {
            notificationContentText = getTimerCountdownTxt().getText().toString();
        }

        Bitmap background = BitmapFactory.decodeResource(this.getActivity().getResources(), R.drawable.ic_tomato);

//        NotificationCompat.Action action =
//                new NotificationCompat.Action.Builder(R.drawable.ic_refresh, "Restart", pendingRestartIntent).build();

        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this.getActivity())
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setLargeIcon(background)
                        .setContentTitle(getString(R.string.app_name))
                        .setContentText(status)
                        .setContentIntent(viewPendingIntent);
//                        .setStyle(new NotificationCompat.BigPictureStyle());

//                        .addAction(R.drawable.ic_refresh, "Restart", pendingRestartIntent);
//                        .extend(new NotificationCompat.WearableExtender().addAction(action))

        // Get an instance of the NotificationManager service
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this.getActivity());

        // Build the notification and issues it with notification manager.
        notificationManager.notify(notificationId, notificationBuilder.build());
    }
}
