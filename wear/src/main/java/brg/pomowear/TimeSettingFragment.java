package brg.pomowear;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.wearable.view.CircledImageView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TimeSettingFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TimeSettingFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class TimeSettingFragment extends Fragment {

    private Integer sessionLength;
    private TextView sessionLengthTextView;
    private Boolean longClicked = false;

    private Timer increaseTimer;
    private Timer decreaseTimer;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment TimeSettingFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TimeSettingFragment newInstance(String param1, String param2) {
        TimeSettingFragment fragment = new TimeSettingFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public static TimeSettingFragment newInstance() {
        TimeSettingFragment fragment = new TimeSettingFragment();
        return fragment;
    }

    public TimeSettingFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_time_setting, container, false);
        sessionLengthTextView = (TextView)v.findViewById(R.id.time_string);
        SharedPreferences sp = this.getActivity().getSharedPreferences("brg.pomowear", Context.MODE_PRIVATE);
        sessionLength = sp.getInt("DEFAULT_SESSION_LENGTH", 25);
        sessionLengthTextView.setText(String.format("%d", sessionLength));

        saveDefaultTime();

        CircledImageView increaseTimeBtn = (CircledImageView)v.findViewById(R.id.increase_time_btn);
        increaseTimeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (longClicked) {
                    longClicked = false;
                } else {
                    sessionLength += 1;
                    updateSessionLengthText();
                    saveDefaultTime();
                }
            }
        });
        increaseTimeBtn.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                startIncreasingTime();
                return false;
            }
        });
        increaseTimeBtn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_UP:
                        stopIncreasingTime();
                        return false;
                }
                return false;
            }
        });

        CircledImageView decreaseTimeBtn = (CircledImageView)v.findViewById(R.id.decrease_time_btn);
        decreaseTimeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (longClicked) {
                    longClicked = false;
                } else {
                    sessionLength -= 1;
                    sessionLength = Math.max(sessionLength, 1);
                    updateSessionLengthText();
                    saveDefaultTime();
                }
            }
        });
        decreaseTimeBtn.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                startDecreasingTime();
                return false;
            }
        });
        decreaseTimeBtn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch(event.getAction()) {
                    case MotionEvent.ACTION_UP:
                        stopDecreasingTime();
                        return false;
                }
                return false;
            }
        });

        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }

    private void updateSessionLengthText() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                sessionLengthTextView.setText(String.format("%d", sessionLength));
            }
        });
    }

    private void startIncreasingTime() {
        int period = 500;
        longClicked = true;

        increaseTimer = new Timer();
        increaseTimer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                Integer increaseAmt = 5 - (sessionLength % 5);
                if (increaseAmt == 0) {
                    increaseAmt = 5;
                }

                sessionLength += increaseAmt;

                updateSessionLengthText();
            }
        }, 0, period);
    }

    private void stopIncreasingTime() {
        if (increaseTimer != null) {
            increaseTimer.cancel();
            increaseTimer.purge();
        }

        saveDefaultTime();
    }

    private void startDecreasingTime() {
        int period = 500;
        longClicked = true;

        decreaseTimer = new Timer();
        decreaseTimer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                Integer decreaseAmt = sessionLength % 5;
                if (decreaseAmt == 0) {
                    decreaseAmt = 5;
                }

                sessionLength -= decreaseAmt;
                if (sessionLength <= 1) {
                    sessionLength = 1;
                    stopDecreasingTime();
                }

                updateSessionLengthText();
            }
        }, 0, period);
    }

    private void stopDecreasingTime() {
        if (decreaseTimer != null) {
            decreaseTimer.cancel();
            decreaseTimer.purge();
        }

        saveDefaultTime();
    }

    public Integer getSessionLength() {
        return sessionLength;
    }

    public void saveDefaultTime() {
        if (this.getActivity() != null) {
            SharedPreferences sp = this.getActivity().getSharedPreferences("brg.pomowear", Context.MODE_PRIVATE);
            sp.edit().putInt("DEFAULT_SESSION_LENGTH", sessionLength).commit();
        }
    }
}
