package brg.pomowear;

import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.view.ViewPager;
import android.support.wearable.view.WatchViewStub;
import android.util.Log;
import android.view.DragEvent;
import android.view.View;
import android.view.ViewGroup;

public class MainWearActivity extends FragmentActivity implements TimerFragment.OnFragmentInteractionListener, TimeSettingFragment.OnFragmentInteractionListener {

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    private enum ViewPage {
        MAIN,
        DEFAULT_TIME_SETTING,
        NUM_PAGES
    }

    private ViewPager mPager;
    private MainAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_wear);

        final WatchViewStub stub = (WatchViewStub) findViewById(R.id.watch_view_stub);
        stub.setOnLayoutInflatedListener(new WatchViewStub.OnLayoutInflatedListener() {
            @Override
            public void onLayoutInflated(WatchViewStub stub) {
                mAdapter = new MainAdapter(getSupportFragmentManager());
                mPager = (ViewPager) stub.findViewById(R.id.pager);
                mPager.setAdapter(mAdapter);
                mPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(int i, float v, int i2) {}

                    @Override
                    public void onPageSelected(int i) {
                        if (i == ViewPage.MAIN.ordinal()) {
                            new TimerFragmentUpdate().execute();
                        }
                    }

                    @Override
                    public void onPageScrollStateChanged(int i) {}
                });

                sendPomoNotification();
            }
        });
    }

    public class TimerFragmentUpdate extends AsyncTask<Void, String, TimerFragment> {

        @Override
        protected TimerFragment doInBackground(Void... voids) {
            TimerFragment timerFragment = null;

            for (Fragment fragment : getSupportFragmentManager().getFragments()) {
                if (fragment instanceof  TimerFragment) {
                    timerFragment = (TimerFragment)fragment;
                    break;
                }
            }

            return timerFragment;
        }

        @Override
        protected void onPostExecute(TimerFragment s) {
            super.onPostExecute(s);
            if (s != null) {
                s.setTimerCountdownTextDefault();
            }
        }
    }

    public class MainAdapter extends FragmentPagerAdapter {
        public MainAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return ViewPage.NUM_PAGES.ordinal();
        }

        @Override
        public Fragment getItem(int position) {
            ViewPage page = ViewPage.MAIN;

            if (position >= 0 && position < ViewPage.NUM_PAGES.ordinal()) {
                page = ViewPage.values()[position];
            }

            Fragment tbr;

            switch (page) {
                case MAIN:
                default:
                    tbr = TimerFragment.newInstance();
                    break;
                case DEFAULT_TIME_SETTING:
                    tbr = TimeSettingFragment.newInstance();
                    break;
            }

            return tbr;
        }

        @Override
        public void setPrimaryItem(ViewGroup container, int position, Object object) {
            super.setPrimaryItem(container, position, object);
        }

    }

    private void sendPomoNotification() {
        int notificationId = 1001;
        // Build intent for notification content
        Intent notificationIntent = new Intent(this, MainWearActivity.class);
        notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent viewPendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

        TimerFragment timerFragment = (TimerFragment)mAdapter.getItem(ViewPage.MAIN.ordinal());
        String notificationContentText = "";
        if (timerFragment != null && timerFragment.getTimerCountdownTxt() != null) {
            notificationContentText = timerFragment.getTimerCountdownTxt().getText().toString();
        }

        Bitmap background = BitmapFactory.decodeResource(this.getResources(), R.drawable.ic_tomato);

        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setLargeIcon(background)
                        .setContentTitle(getString(R.string.app_name))
                        .setContentText(notificationContentText)
                        .setContentIntent(viewPendingIntent);
//                        .setStyle(new NotificationCompat.BigPictureStyle());

        // Get an instance of the NotificationManager service
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);

        // Build the notification and issues it with notification manager.
        notificationManager.notify(notificationId, notificationBuilder.build());
    }
}
